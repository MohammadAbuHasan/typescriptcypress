// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add('login',() =>{
// cy.request(
//     {
//         method:'POST',
//         url: 'http://account.booking.com/sign-in',
//         body:{
//             user:{
//                 username: 'mohammadabuhasan155@gmail.com',
//                 password: 'hihihicry',

//             }
//         }

//     }
// )
// .then((resp) =>{
//     window.localStorage.setItem('jwt',resp.body.user)
// })
        cy.visit('https://www.booking.com/')
        // click on sign in 
        cy.get('#current_account > .popover_trigger > .sign_in_wrapper').click({force: true})
        // type an invalid username  
        cy.get('#username').clear().type('mohammadabuhasan155@gmail.com')
        // click on Next Button 
        cy.get('.bui-button__text').click()
        cy.get('#password').clear().type('hihihicry')
        // Click on sign in button
        cy.get('.bui-button__text').click()
        // wait until move to the book homepage
        cy.wait(5000)
        // make sure that move to the next page
        cy.url().should('eq', 'https://www.booking.com/?auth_success=1')
        
})
Cypress.Commands.add('login2',() =>{
    cy.request(
            {
                method:'POST',
                url: 'http://account.booking.com/sign-in',
                body:{
                    user:{
                        username: 'mohammadabuhasan155@gmail.com',
                        password: 'hihihicry',
        
                    }
                }
        
            
            }
        )
        .then((resp) =>{
            window.localStorage.setItem('jwt',resp.body.user)
            
        })

  //     cy.visit('https://www.booking.com/')
//      // click on sign in 
//   cy.get('#current_account > .popover_trigger > .sign_in_wrapper').click({force: true})
// // fill valid  username
// cy.get('#username').type('mohammadabuhasan155@gmail.com')
// // click on Next Button 
// cy.get('.bui-button__text').click()
// // valid password
// cy.get('#password').clear().type('hihihicry')
// // Click on sign in button
// cy.get('.bui-button__text').click()
// // wait until move to the book homepage
