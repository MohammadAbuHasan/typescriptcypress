/// <reference types="Cypress" />
import testData from "../fixtures/testData.json";

describe("Sign in", () => {
  beforeEach(() => {
    cy.visit("https://www.booking.com/");
    // click on sign in
    cy.get("#current_account > .popover_trigger > .sign_in_wrapper").click({
      force: true,
    });
    // cy.get('#username').clear()
  });

  testData.forEach((testDataRow: any) => {
    let data = {
      email: testDataRow.email,
      password: testDataRow.password,
      isvalidemail: testDataRow.isvalidemail,
      isvalidpassword: testDataRow.isvalidpassword,
    };

    if (data.email == "" && data.isvalidemail==false) {
      it("Test empty username "+ data.email +" "+ data.password, () => {
        // click on Next Button
        cy.get(".bui-button__text").click();
        // Make sure  that  the  response massege is  'Enter your email address'
        cy.get("#username-error")
          .invoke("text")
          .should((message) => {
            expect(message).to.match(/Enter your email address/);
          });
      });
    } else if (data.email != "" && data.isvalidemail == false) {
      it("Test with invalid username "+ data.email +" "+ data.password, () => {
        // type an invalid username
        cy.get("#username").type(data.email);
        // click on Next Button
        cy.get(".bui-button__text").click();
        // Make sure  that return an error message
        cy.get("#username-error")
          .invoke("text")
          .should((message) => {
           
            expect(message).to.match(/Looks like there isn't an account associated with this email address. You can create an account to access our services./);
          });
      });
    } else if (
      data.email != "" &&
      data.isvalidemail == true &&
      data.password == "" && data.isvalidemail==false
    ) {
      it("Test with valid username and empty password "+ data.email +" "+ data.password, () => {
        // type an valid username
        cy.get("#username").clear().type(data.email);
        // click on Next Button
        cy.get(".bui-button__text").click();
        // Empty password
        cy.get("#password").clear();
        // Click on sign in button
        cy.get(".nw-signin > .bui-button").click();
        // Make sure  that  return  an error message
        cy.get("#password-error")
          .invoke("text")
          .should((message) => {
            expect(message).to.match(/Enter your Booking.com password/);
          });
      });
    } else if (
      data.email != "" &&
      data.isvalidemail == true &&
      data.password != "" &&
      data.isvalidpassword == false
    ) {
      it("Test with valid username and invalid password "+ data.email +" "+ data.password, () => {
        // fill valid  username
        cy.get("#username").type(data.email);
        // click on Next Button
        cy.get(".bui-button__text").click();
        // invalid password
        cy.get("#password").clear().type(data.password);
        // Click on sign in button
        cy.get(".bui-button__text").click();
        // Make sure  that  return  an error message
        cy.get("#password-error")
          .invoke("text")
          .should((message) => {
            expect(message).to.match(/The email and password combination you entered doesn't match./);
          });
      });
    } else if (
      data.email != "" &&
      data.isvalidemail == true &&
      data.password != "" &&
      data.isvalidpassword == true
    ) {
      it("Test with valid username and valid password "+ data.email +" "+ data.password, () => {
        // fill valid  username
        cy.get("#username").type(data.email);
        // click on Next Button
        cy.get(".bui-button__text").click();
        // valid password
        cy.get("#password").clear().type(data.password);
        // Click on sign in button
        cy.get(".bui-button__text").click();
        // wait until move to the book homepage
        cy.wait(5000);
        // make sure that move to the next page
        cy.url().should("eq", "https://www.booking.com/?auth_success=1");
        // click on Profile
        cy.get("#current_account > .popover_trigger").click({ force: true });
        cy.get(".profile-menu__item--signout > .profile-menu__link").click({
          force: true,
        });
      });
    }
   
  });
});
