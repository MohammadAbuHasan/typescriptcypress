
/// <reference types="Cypress" />

  describe('Search checking ', () => {
    beforeEach(() => {
        cy.login()
         
    });



    it('Search without  Anything', ()=>{
    
        
        cy.get('#ss').click()
        cy.get('#ss').clear()
        cy.get('.sb-searchbox__button > span:nth-child(1)').click()
        cy.get('#frm').submit()
        cy.get('.fe_banner__message').invoke('text').should((message) =>{
            expect(message).to.match(/Enter a destination to start searching./)
        
        
     
    
        })
     } );
   



     it('Search with City Name  and  without   reservation date selection  ', ()=>{
     cy.get('#ss').clear()
     cy.get('#ss').click()
     cy.get('.sb-autocomplete__item--with-title').click()
     cy.get('.sb-searchbox__button').click()
     cy.get('#frm').submit();
     cy.url().should('contains', 'https://www.booking.com/searchresults.html')
     

        

     });



     it('Search with City Name  and  with reservation date  ', ()=>{
        
        cy.get('#ss').clear()
        cy.get('#ss').click()
        cy.get('.sb-autocomplete__item--with-title').click()
        cy.get('.bui-calendar__wrapper:nth-child(1) .bui-calendar__row:nth-child(3) > .bui-calendar__date:nth-child(1) > span > span').click()
        cy.get('.bui-calendar__wrapper:nth-child(1) .bui-calendar__row:nth-child(3) > .bui-calendar__date:nth-child(4) > span > span').click()
        cy.get('.sb-searchbox__button').click()
        cy.get('#frm').submit()
        cy.url().should('contains', 'https://www.booking.com/searchresults.html')
        
   
        });




        it('Search with City Name  and  with reservation date  and with adult number  ', ()=>{
        
            
            cy.get('#ss').clear()
            cy.get('#ss').click()
            cy.get('.sb-autocomplete__item--with-title > span:nth-child(2)').click()
            cy.get('.bui-calendar__wrapper:nth-child(1) .bui-calendar__row:nth-child(3) > .bui-calendar__date:nth-child(1)').click()
            cy.get('.bui-calendar__wrapper:nth-child(1) .bui-calendar__row:nth-child(3) > .bui-calendar__date:nth-child(4)').click()
            cy.get('.xp__guests__count > span:nth-child(1)').click()
            cy.get('.sb-group__field-adults .bui-stepper__subtract-button > .bui-button__text').dblclick()
            cy.get('.sb-group__field-adults .bui-stepper__subtract-button > .bui-button__text').click()
            cy.get('.sb-group__field-adults .bui-stepper__subtract-button > .bui-button__text').click()
            cy.get('.sb-group__field-adults .bui-stepper__subtract-button > .bui-button__text').click()
            cy.get('.sb-group__field-adults .bui-stepper__subtract-button > .bui-button__text').click()
            cy.get('.sb-group__field-adults .bui-stepper__subtract-button > .bui-button__text').click()
            cy.get('.sb-group__field-adults .bui-stepper__subtract-button > .bui-button__text').click()
            cy.get('.sb-group__field-adults .bui-stepper__subtract-button > .bui-button__text').click()
            cy.get('.sb-searchbox__button > span:nth-child(1)').click()
            cy.get('#frm').submit()
            cy.url().should('contains', 'https://www.booking.com/searchresults.html');
            
            });


            it('Search with City Name  and  with   reservation date selection, Adult,Chidren,Rooms ', ()=>{
                cy.get('#ss').clear()
               cy.get('#ss').click()
               cy.get('.sb-autocomplete__item--with-title').click()
               cy.get('.bui-calendar__wrapper:nth-child(1) .bui-calendar__row:nth-child(3) > .bui-calendar__date:nth-child(2)').click()
                cy.get('.bui-calendar__wrapper:nth-child(1) .bui-calendar__row:nth-child(3) > .bui-calendar__date:nth-child(3)').click()
                 cy.get('.xp__guests__count > span:nth-child(1)').click()

                  cy.get('.sb-group-children .bui-stepper__add-button > .bui-button__text').click()

              cy.get('.xp__guests__count > span:nth-child(2) > span').click()
             cy.get('.sb-group__field-rooms .bui-stepper__add-button').click()
             cy.get('.sb-group-children .bui-stepper__add-button > .bui-button__text').click()
           cy.get('.sb-group-children .bui-stepper__subtract-button').click()
            cy.get('#xp__guests__toggle').click()
       cy.get('.sb-group__field-rooms .bui-stepper__add-button').click()
          cy.get('.sb-group__field-rooms .bui-stepper__add-button').click()
             cy.get('.sb-searchbox__button > span:nth-child(1)').click()
                    cy.get('#frm').submit()
                   cy.url().should('contains', 'https://www.booking.com/searchresults.html')

           
                   
           
                });


                it('Search with Max number of   Adult,Chidren,Rooms  ', ()=>{

                 cy.get('#ss').clear();
                cy.get('#ss').click();
                cy.get('.sb-autocomplete__item--with-title').click();
                cy.get('.xp__input-group:nth-child(2) .sb-date-field__icon').click();
                cy.get('.xp__input-group:nth-child(2) .sb-date-field__icon').click();
                cy.get('.bui-calendar__wrapper:nth-child(1) .bui-calendar__row:nth-child(3) > .bui-calendar__date:nth-child(4) > span > span').click();
                cy.get('.bui-calendar__wrapper:nth-child(1) .bui-calendar__row:nth-child(3) > .bui-calendar__date:nth-child(6) > span > span').click();
                cy.get('#xp__guests__toggle').click();
                cy.get('div:nth-child(1) > .u-clearfix').click();
                cy.get('.sb-group__stepper-button-disabled').click();
                  
                 cy.get('.sb-group-children .bui-stepper__add-button').click();
                 cy.get('.sb-group__field-rooms .bui-stepper__add-button > .bui-button__text').click();
                  cy.get('.sb-searchbox__button').click();
                   cy.get('#frm').submit();
                  cy.url().should('contains', 'https://www.booking.com/searchresults.html');

                    });




                    
                     




    }
  )